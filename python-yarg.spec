%global _empty_manifest_terminate_build 0
Name:		python-yarg
Version:	0.1.9
Release:	2
Summary:	A semi hard Cornish cheese, also queries PyPI (PyPI client)
License:	MIT
URL:		https://yarg.readthedocs.org/
Source0:	https://files.pythonhosted.org/packages/d4/c8/cc640404a0981e6c14e2044fc64e43b4c1ddf69e7dddc8f2a02638ba5ae8/yarg-0.1.9.tar.gz
BuildArch:	noarch


%description
%{summary}

%package -n python3-yarg
Summary:	A semi hard Cornish cheese, also queries PyPI (PyPI client)
Provides:	python-yarg
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-yarg
%{summary}

%package help
Summary:	Development documents and examples for yarg
Provides:	python3-yarg-doc
%description help
%{summary}

%prep
%autosetup -n yarg-0.1.9

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-yarg -f filelist.lst
%dir %{python3_sitelib}/*
%exclude %{python3_sitelib}/tests

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Feb 27 2023 xu_ping <xuping33@h-partners.com> - 0.1.9-2
- Remove tests files that conflict with python-prometheus-api-client.

* Mon Aug 24 2020 Python_Bot <Python_Bot@openeuler.org> - 0.1.9-1
- Package Spec generated
